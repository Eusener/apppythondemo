from django.forms import ModelForm, TextInput
from .models import Cidades

class CidadeForm(ModelForm):
    class Meta:
        model = Cidades
        fields = ['nome']
        widgets = {'nome' : TextInput(attrs={'class' : 'input', 'placeholder' : 'Nome da Cidade'})}