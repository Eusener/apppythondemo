import requests
from django.shortcuts import render
from .models import Cidades
from .forms import CidadeForm
def index(request):
    url = 'http://api.openweathermap.org/data/2.5/weather?q={}&units=metric&appid=b9483a324940896e4829c06b90926f0b'
    
    """
    A API obriga o uso de palavras reservadas, city é um exemplo, detesto escrever codigo misturando portugues 
    e Ingles, provavelmente no futuro eu use somente ingles :/, PS:detesto colocar acentos nas palavras
    """
    
    if request.method == 'POST':
        form = CidadeForm(request.POST)
        # Salvar no banco sem escrever uma linha do sql! ORM é vida!
        form.save()

    form = CidadeForm()
    # Buscando todos registros do banco ! ORM :)
    cidades = Cidades.objects.all()
    clima_das_cidades = []
    
    for city in cidades:
        
        req = requests.get(url.format(city)).json()
        
        clima_cidade = {
            'cidade' : city.nome,
            'temperatura' : req['main']['temp'],
            'info' : req['weather'][0]['description'],
            'icone' : req['weather'][0]['icon'],
        }
        clima_das_cidades.append(clima_cidade)
    # Saida no terminal do array com os dados para demonstração, versão de produção seria legal remover isso
    print(clima_das_cidades)

    #Contexto é um dicionario do tipo chave valor, onde pode ser exibido pelo html (renderizado pelo DTE)
    context = {'clima_das_cidades' : clima_das_cidades, 'form' : form}

    return render(request, 'clima.html', context)
